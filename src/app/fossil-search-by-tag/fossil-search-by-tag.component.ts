import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Filter, FossilService, TagFilter} from '../fossils/fossil.service';
import {Fossil} from '../fossils/fossil';

@Component({
  selector: 'app-fossil-search-by-tag',
  templateUrl: './fossil-search-by-tag.component.html',
  styleUrls: ['./fossil-search-by-tag.component.css']
})
export class FossilSearchByTagComponent implements OnInit {

  @Output() addFossil = new EventEmitter<Fossil>();

  public tagFiltersByName: TagFilter;

  constructor(private fossils: FossilService) { }

  ngOnInit() {
    this.tagFiltersByName = {};
  }


  public fossilsMatchingFilters(): Fossil[] {
    return this.fossils.fossilsMatchingFilters(this.tagFiltersByName);
  }

  public get tags(): string[] {
    if (Object.keys(this.tagFiltersByName).length === 0) {
      this.fossils.tags.forEach((tag) => {
        this.tagFiltersByName[tag] = Filter.neutral;
      });
    }
    return Array.from(this.fossils.tags).sort();
  }

  public resetTagFilters(): void {
    for (const tag of this.tags) {
      this.tagFiltersByName[tag] = Filter.neutral;
    }
  }

  public triggerAddFossil(fossil: Fossil): void {
    this.addFossil.emit(fossil);
  }
}
