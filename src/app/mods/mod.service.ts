import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Mod, ModTemplate} from './mod';
import {Observable} from 'rxjs';
import {Fossil} from '../fossils/fossil';

@Injectable({
  providedIn: 'root'
})
export class ModService {

  public loaded: boolean;
  public mods: Mod[];
  public modsByTag: {[tag: string]: Mod[]};

  protected memoized: {[memoKey: string]: Mod[]};
  protected allTags: Set<string>;

  constructor(
    private http: HttpClient,
  ) {
    this.loaded = false;
    this.mods = [];
    this.modsByTag = {};
    this.memoized = {};
    this.allTags = new Set;
    this.getJson().subscribe(data => {
      this.mods = this.postProcess(data);
      this.loaded = true;
    });
  }

  public likelyModsByFossils(fossils: Fossil[]): Mod[] {
    fossils = this.relevantFossils(fossils);
    const memoized = this.memoizedResults('likely', fossils);
    if (memoized !== undefined) { return memoized; }
    const result = [];
    const forbidden = this.allForbiddenTags(fossils);
    for (const tag of this.allLikelyTags(fossils)) {
      // Extra filtering work because mods can have multiple tags,
      // so we might look up a likely one by one tag (e.g. 'fossil')
      // and then disqualify it by another (e.g. 'lightning')
      const mods = this.modsByTag[tag].filter(mod => {
        for (const modTag of mod.tags) {
          if (forbidden.has(modTag)) { return false; }
        }
        return true;
      });
      result.push(...mods);
    }

    this.memoizeResults('likely', fossils, result);
    return result;
  }

  public stillPossibleModsByFossils(fossils: Fossil[]): Mod[] {
    fossils = this.relevantFossils(fossils);
    const memoized = this.memoizedResults('possible', fossils);
    if (memoized !== undefined) { return memoized; }
    const result = [];

    const forbidden = this.allForbiddenTags(fossils);
    const likely = this.allLikelyTags(fossils);
    const less = this.lessLikelyTags(fossils);

    for (const tag of this.stillPossibleTags(fossils)) {
      let mods = this.modsByTag[tag];
      if (!mods) { continue; }
      mods = this.modsByTag[tag].filter(mod => {
        if (mod.tags.includes("fossil")) {
          // Fossil mods can't appear anywhere but in 'likely' and
          // 'forbidden'.  The exceptions are 'Aetheric' and 'Serrated'
          // which have 'fewer' mods.  They get lumped into this category.
          if (!this.modHasAnyTag(mod, likely)) { return false; }
        }
        for (const modTag of mod.tags) {
          if (forbidden.has(modTag)) { return false; }
          if (likely.has(modTag)) { return false; }
        }
        return true;
      });
      result.push(...mods);
    }
    this.memoizeResults('possible', fossils, result);
    return result;
  }

  public forbiddenModsByFossils(fossils: Fossil[]): Mod[] {
    fossils = this.relevantFossils(fossils);
    const memoized = this.memoizedResults('forbidden', fossils);
    if (memoized !== undefined) { return memoized; }
    const result = [];

    for (const tag of this.allForbiddenTags(fossils)) {
      result.push(...this.modsByTag[tag]);
    }
    this.memoizeResults('forbidden', fossils, result);
    return result;
  }

  public relevantFossils(fossils: Fossil[]): Fossil[] {
    return fossils.filter(fossil => {
      if (fossil === undefined) { return false; }
      return fossil.tagWeights.length > 0;
    }).sort((fossil1, fossil2) => {
      return fossil1.name.localeCompare(fossil2.name);
    });
  }

  protected allForbiddenTags(fossils: Fossil[]): Set<string> {
    const result = new Set();
    for (const fossil of fossils) {
      for (const tag of fossil.forbiddenTags()) {
        result.add(tag);
      }
    }
    return result;
  }

  protected allLikelyTags(fossils: Fossil[]): Set<string> {
    const result = new Set();
    const forbidden = this.allForbiddenTags(fossils);
    for (const fossil of fossils) {
      for (const tag of fossil.likelyTags()) {
        if (!forbidden.has(tag)) { result.add(tag); }
      }
    }
    return result;
  }

  protected lessLikelyTags(fossils: Fossil[]): Set<string> {
    const result = new Set();
    const forbidden = this.allForbiddenTags(fossils);
    for (const fossil of fossils) {
      for (const tag of fossil.lessLikelyTags()) {
        if (!forbidden.has(tag)) { result.add(tag); }
      }
    }
    return result;
  }

  protected stillPossibleTags(fossils: Fossil[]): Set<string> {
    const result = new Set();
    const all = this.allTags;
    const forbidden = this.allForbiddenTags(fossils);
    const likely = this.allLikelyTags(fossils);
    for (const tag of all) {
      if (!(forbidden.has(tag) || likely.has(tag))) {
        result.add(tag);
      }
    }
    return result;
  }

  protected getJson(): Observable<ModTemplate[]> {
    return this.http.get<ModTemplate[]>("assets/resonatable_mods.json");
    // return this.http.get<ModTemplate[]>("assets/mods-full.json");
  }

  protected modHasAnyTag(mod: Mod, tags: Set<string>): boolean {
    for (const tag of mod.tags) {
      if (tags.has(tag)) { return true; }
    }
    return false;
  }

  protected memoizeKey(category: string, fossils: Fossil[]) {
    const fossilKey = fossils.map(f => f.name).join("+");
    return `[${category}]${fossilKey}`;
  }

  protected memoizeResults(category: string, fossils: Fossil[], mods: Mod[]) {
    // Don't memoize until everything's loaded
    if (this.allTags.size === 0) { return; }
    const key = this.memoizeKey(category, fossils);
    this.memoized[key] = mods;
    // console.log(`Stored ${key}`, mods);
  }

  protected memoizedResults(category: string, fossils: Fossil[]): Mod[] {
    const key = this.memoizeKey(category, fossils);
    const result = this.memoized[key];
    // console.log(`Retrieved ${key}`, result);
    return result;
  }

  protected postProcess(data: ModTemplate[]): Mod[] {
    const allData = data.map(mod => {
      const result = new Mod(mod);
      for (const tag of result.tags) {
        if (this.modsByTag[tag] === undefined) { this.modsByTag[tag] = []; }
        this.modsByTag[tag].push(result);
        this.allTags.add(tag);
      }
      return result;
    });
    return allData;
  }

}
