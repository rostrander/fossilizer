import {Component, Input, OnInit} from '@angular/core';
import {ModService} from '../mods/mod.service';
import {Fossil} from '../fossils/fossil';
import {FossilService} from '../fossils/fossil.service';
import {Mod} from '../mods/mod';
import {BaseService} from '../mods/base.service';

@Component({
  selector: 'app-mod-results',
  templateUrl: './mod-results.component.html',
  styleUrls: ['./mod-results.component.css']
})
export class ModResultsComponent implements OnInit {

  public currentBase: string;
  public minLevel: number;

  @Input() fossilNames: string[];

  constructor(
    private modService: ModService,
    private fossilService: FossilService,
    private baseService: BaseService,
  ) {

  }

  ngOnInit() {
    this.minLevel = 70;
  }

  public get currentFossils(): Fossil[] {
    const fossils = this.fossilNames.map(name =>
      this.fossilService.fossilsByName[name]);

    return fossils;
  }

  public forbiddenModsForCurrentFossils(): Mod[] {
    return this.modService.forbiddenModsByFossils(this.currentFossils);
  }

  public likelyModText(): Set<string> {
    const result = new Set();
    let likely = this.modService.likelyModsByFossils(this.currentFossils);
    likely = this.filterModsByCurrentBase(likely);
    for (const mod of likely) {
      result.add(mod.descriptionWithAffix);
    }
    return result;
  }

  public stillPossibleModText(): Set<string> {
    const result = new Set();
    let possible = this.modService.stillPossibleModsByFossils(this.currentFossils);
    possible = this.filterModsByCurrentBase(possible);
    for (const mod of possible) {
      result.add(mod.descriptionWithAffix);
    }
    return result;
  }

  public forbiddenModText(): Set<string> {
    const result = new Set();
    let forbidden = this.forbiddenModsForCurrentFossils();
    forbidden = this.filterModsByCurrentBase(forbidden);
    for (const mod of forbidden) {
      result.add(mod.descriptionWithAffix);
    }
    return result;
  }

  public addSomeFossils(): boolean {
    // TODO: Differentiate between no input and everything being ruled out
    return this.modService.likelyModsByFossils(this.currentFossils).length === 0;
  }

  public pickABase(): boolean {
    return this.currentBase === undefined || this.currentBase === '';
  }

  public allIsPermitted(): boolean {
    return this.forbiddenModsForCurrentFossils().length === 0;
  }

  public allBaseNames(): string[] {
    return this.baseService.bases.map(base => base.base);
  }

  protected filterModsByCurrentBase(mods: Mod[]): Mod[] {
    let result = mods;
    if (!this.currentBase) {
      result = result.filter( mod => mod.required_level <= this.minLevel);
    } else {
      const baseTags = this.baseService.tagsForBase(this.currentBase);
      result = result.filter(mod => {
        return mod.bases.some(modbase => {
          return baseTags.includes(modbase);
        }) && mod.required_level <= this.minLevel;
      });
    }
    return result.sort((mod1, mod2) => {
      return mod1.descriptionWithAffix.localeCompare(mod2.descriptionWithAffix);
    });
  }

}
