import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Fossil, FossilTemplate} from './fossil';

interface FossilLookup {
  [key: string]: Fossil[];
}

@Injectable({
  providedIn: 'root'
})
export class FossilService {

  public loaded: boolean;
  public fossils: Fossil[];

  public tags: Set<string>;
  public fossilsIncludeByTag: FossilLookup;
  public fossilsExcludeByTag: FossilLookup;
  public fossilsByName: {[key: string]: Fossil};

  constructor(private http: HttpClient) {
    this.loaded = false;
    this.fossils = [];
    this.tags = new Set();
    this.fossilsExcludeByTag = {};
    this.fossilsIncludeByTag = {};
    this.fossilsByName = {};
    this.getJson().subscribe(data => {
      this.fossils = this.postProcess(data);
      this.loaded = true;
    });
  }

  public fossilsMatchingFilters(filters: TagFilter): Fossil[] {
    let matches = [];
    for (const tag of Object.keys(filters)) {
      if (filters[tag] === Filter.include) {
        matches.push(...this.fossilsIncludeByTag[tag]);
      } else if (filters[tag] === Filter.exclude) {
        matches.push(...this.fossilsExcludeByTag[tag]);
      }
    }

    if (matches.length === 0) {
      matches = this.fossils;
    }

    return matches;
  }

  protected getJson(): Observable<Fossil[]> {
    return this.http.get<Fossil[]>("assets/fossils.json");
  }

  protected postProcess(data: FossilTemplate[]): Fossil[] {
    return data.map(fossil => {
      const result = new Fossil(fossil);
      for (const tagWeight of fossil.tagWeights) {
        this.tags.add(tagWeight.tag);
        const tag = tagWeight.tag;
        this.fossilsIncludeByTag[tag] = this.fossilsIncludeByTag[tag] || [];
        this.fossilsExcludeByTag[tag] = this.fossilsExcludeByTag[tag] || [];
        if (tagWeight.weight >= 1) {
          this.fossilsIncludeByTag[tagWeight.tag].push(result);
        } else {
          this.fossilsExcludeByTag[tag].push(result);
        }
      }
      this.fossilsByName[fossil.name] = result;
      return result;
    });
  }
}

export enum Filter {
  include = 'include',
  exclude = 'exclude',
  neutral = 'neutral',
}

export interface TagFilter {
  [tag: string]: Filter;
}
