
* For the Tangled Fossil, it looks like all of the fossil modifiers have
"domain": "delve" - I'm going to postprocess that into a 'fossil' tag.

* Some fossils don't have tags on poedb but their mods do appear to be tagged:
  * Bloodstained Fossil => "vaal"
* Some fossils have 'fewer' rather than 'no', I'm putting setting 0 < weight < 1 for them:
  * Aetheric ("Fewer attack modifiers")
  * Serrated ("Fewer caster modifiers")
  * Pristine has x0.1 'flat life regen' but this is clearly 'more' so I'm setting it to 100
* Some fossils give mods that don't have tags, going to postprocess them in:
  * Hollow Fossil => "abyssal_socket"  (DelveAbyssJewelSocket1)
  * Gilded Fossil => "gilded" (anything with stat id local_item_sell_price_doubled)
* And some have no tags but definitely have effects we'd like to list.  For now, I'm just
  not doing that, but putting it on the list for potential future expansion:
  * Glyphic Fossil: Has a Corrupt Essence modifier
  * Enchanted Fossil: Has a Labyrinth Enchantment

* Note that any mod that had to be post-processed should not show up without
  fossil crafting (e.g. by using a chaos orb).  Going to need to make sure delve mods cannot
  be applied without a fossil that grants them being used.  Basically this applies to the
  'less likely but still possible' column - anything in 'likely' has been plussed-in by a fossil,
  anything in 'forbidden' has been canceled.  Probably need 'delve by tag' and 'nondelve by tag'
  dictionaries for easy lookup.

* Just using the bases isn't enough.  Something like 'Goathide Boots' has four tags,
  "dex_armour", "boots", "armour", and "default".  Probably need to move from bases to
  a 'base combo' sort of thing (e.g. 'dex_armour/boots/armour').  A mod that applies to
  any of them appears.  Downside is that there's some unexpected interactions
  (e.g. my tool will claim you'll roll +armor/ES on a dex armour because those mods
  are on body_armour for some reason).  Don't see anything in the data that rules that out.
  * Note that none of these base items have the various 'shaper'/'elder' tags.
    Can probably just ignore them for now.

----
3.4.2 changelog:

Lucent and Shuddering Fossils now have special modifiers that can be rolled on shields.
Poison mods are now considered Chaos mods for the purposes of Fossil crafting.
Bleeding mods are now considered Physical mods for the purposes of Fossil crafting.
