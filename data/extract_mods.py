#!/usr/bin/env python
import json

DOMAINS = [
    "delve",
    "item",
]

GENERATION = [
    "prefix",
    "suffix",
]

def nonzeroSpawnWeights(weights):
    return [w['tag'] for w in weights if w["weight"]]

def modAppliesToFossilCrafting(data):
    domain = data.get('domain')
    if domain not in DOMAINS:
        print("Rejecting due to domain %s" % domain)
        return False
    generation = data.get('generation_type')
    if generation not in GENERATION:
        print("Rejecting due to generation %s" % generation)
        return False
    spawn_weights = data.get('spawn_weights')
    if not spawn_weights:
        print("Rejecting because it lacks spawn weights")
        return False
    nonzero_weights = nonzeroSpawnWeights(spawn_weights)
    if not nonzero_weights:
        print("Rejecting because all spawn weights are zero")
        return False
    return nonzero_weights

def build_translations(data):
    result = {}
    for entry in data:
        # It looks like the first one is almost always right:
        english = entry["English"][0]["string"].format("#", "#", "#", "#")
        # english = "/".join([strEntry["string"] for strEntry in entry["English"]])
        for id in entry["ids"]:
            result[id] = english

    return result

if __name__ == '__main__':
    with open("data/mods.json", "r") as infile:
        everything = json.load(infile)

    with open("data/stat_translations.json", "r") as infile:
        translations = build_translations(json.load(infile))

    mods = []

    domains = set()
    generation = set()
    levels = set()
    for fullpath, data in everything.items():
        print("Examining %s" % fullpath)
        if modAppliesToFossilCrafting(data):
            tags = data['adds_tags']
            if data['domain'] == 'delve':
                tags.append('fossil')
            if not tags:
                tags.append('none')  # So it'll show up as possible
            descriptions = {translations.get(stat['id'], stat['id']) for stat in data['stats']}
            description = ", ".join(descriptions)
            result = {
                "name": fullpath,
                "description": description,
                "affix": data['generation_type'],
                "required_level": data['required_level'],
                "bases": nonzeroSpawnWeights(data['spawn_weights']),
                "tags":	tags,
            }
            print(fullpath)
            mods.append(result)
    with open("src/assets/mods-full.json", "w") as outfile:
        json.dump(mods, outfile, indent=4)
