#!/usr/bin/env python
import json
import re
from collections import defaultdict

# An item that has any of these tags gets skipped entirely
DISREGARD_TAGS = {
    "not_for_sale",
    "currency",
    "gem",
    "flask",
}

# We ignore these tags entirely
REMOVE_TAGS = {
    "default",      # App assumes this always applies
    "small_staff",  # fixes a collision, no mods apply to this specifically
    "twostonering", # as above
    "talisman",     # ditto
}

CATNAME = re.compile("([a-zA-Z]+)[0-9_]*")

def category_name(fullname):
    last_name = fullname.split("/")[-1]
    return CATNAME.match(last_name).group(1)
    #return fullname.split("/")[-1]

if __name__ == '__main__':
    with open("data/base_items.json", "r") as infile:
        items = json.load(infile)
    combos = defaultdict(set)
    for fullname, data in items.items():
        category = category_name(fullname)
        tags = [tag for tag in data['tags'] if tag not in REMOVE_TAGS]
        if any(tag in DISREGARD_TAGS for tag in tags):
            continue
        tags = "/".join(sorted(tags))
        if tags == "ring/unset_ring":
            category = "UnsetRing"
        combos[tags].add(category)

    revlookup = {}
    for tags, categories in combos.items():
        for category in categories:
            if category in revlookup:
                print("%s already in lookup! %s" % (category, revlookup[category]))
                print("This tags are: %s" % tags)
                raise SystemExit
            revlookup[category] = tags

    data = [
        {
            "base": category,
            "tags": tags.split("/"),
        } for category, tags in revlookup.items()
    ]

    with open("src/assets/bases.json", "w") as outfile:
        json.dump(data, outfile, indent=4)
