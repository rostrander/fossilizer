#!/usr/bin/env python
import json

def isFossil(record):
    # Space rules out "Fossilized spirit shield"
    if " Fossil" in record['name']:
        return True
    if "Delve" in record['visual_identity']['id']:
        return True
        return False

if __name__ == '__main__':
    with open("data/base_items.json", "r") as infile:
        everything = json.load(infile)

    with open("src/assets/fossils.json", "r") as infile:
        result = json.load(infile)

    for fullpath, data in everything.items():
        resid = data['visual_identity']['id']
    if resid not in result and isFossil(data):

        todoTag = {"tag": "TODO", "weight": 0}
        result.append({
            "id": resid,
            "fullId": fullpath,
            "name": data['name'],
            "description": data['properties'].get('description', "UNKNOWN"),
            "tagWeights": [ todoTag ]
        })
        print("Added %s" % data['name'])


    with open("src/assets/fossils.json.new", "w") as outfile:
        json.dump(result, outfile, indent=4)
